package com.pruudkij.hibernate.crud.dao;

import java.util.List;

import com.pruudkij.hibernate.crud.entities.Singer;

/**
 * Created by iuliana.cosmina on 4/21/17.
 */
public interface SingerDao {

	List<Singer> findAll();

	List<Singer> findAllWithAlbum();

	Singer findById(Long id);

	Singer save(Singer singer);

	void delete(Singer singer);
}
