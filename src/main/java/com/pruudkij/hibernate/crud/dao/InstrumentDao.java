package com.pruudkij.hibernate.crud.dao;

import com.pruudkij.hibernate.crud.entities.Instrument;

/**
 * Created by iuliana.cosmina on 4/23/17.
 */
public interface InstrumentDao {

	Instrument save(Instrument instrument);

}
